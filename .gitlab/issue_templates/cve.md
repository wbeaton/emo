<!--
There's help in the Eclipse Foundation Project Handbook https://www.eclipse.org/projects/handbook/#vulnerability-cve

Note that this issue is configured (see the quick actions at the bottom) to be created as confidential.

Note that a vulnerability does not need to actually be resolved before it is reported and that these reports can be revised as needed (reopen the issue to request changes).

You can delete the comments (or not).
-->

The Eclipse Foundation is a [Common Vulnerabilities and Exposures](https://cve.mitre.org/) (CVE) Numbering Authority. This issue it used to request and track the progress of the assignment of a CVE for a vulnerability in the project code for an Eclipse open source project.

<!-- 
Required. Specify the project's name (e.g., "Eclipse Dash") and Eclipse Foundation ID, e.g., "technology.dash". 
-->

## Basic information

**Project name:** {name}

**Project id:** {id}

<!-- 
Required. Specify the version range as precisely as possible, e.g., "[3.0, 3.5.1]" or "[3.0, 3.5.1)". Note that using the standard range notion, square brackets are inclusive (i.e., that version is included in the range), and round brakets are exclusive (the vulnerability affects all versions up to but not including the named version). 

Multiple ranges can be provided.
-->

**Versions affected:** {versions}

<!-- 
Required. The Common Weakness Enumeration (CWE) code comes from here: https://cwe.mitre.org/, e.g., "CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')". Multiple codes can be provided.
-->

**Common Weakness Enumeration:** 

- {[cwe1](https://cwe.mitre.org/)}
- {[cwe2](https://cwe.mitre.org/)}
- ...

<!-- 
Optional. Provide a Common Vulnerability Scoring System (CVSS). Note that if you do not provide this, then some agencies (eg. NIST) will compute it on the project's behalf. Please be sure to include the CVSS version number, e.g., "3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H".

There's help here: https://nvd.nist.gov/vuln-metrics/cvss
-->

**Common Vulnerability Scoring System:** {[cvss](https://nvd.nist.gov/vuln-metrics/cvss)}

<!-- 
Required. The summary should start with the name of the project, e.g., "Eclipse Vert.x", then a description of the affected versions, followed by a description of the problem. The summary should be concise. For example, 

  "In Eclipse Vert.x version 3.0 to 3.5.1, the HttpServer response
   headers and HttpClient request headers do not filter carriage return and
   line feed characters from the header value. This allow unfiltered values
   to inject a new header in the client request or server response."
-->

**Summary:**

In {name} versions {versions}, ...

<!-- 
Required. Include a link to the issue (e.g., GitHub Security Advisory) that's being used to track/resolve the issue. Other links that provide more information can be provided.
-->

**Links:**

- {primary resolution link}

## Tracking

**This section will completed by the project team**.

- [ ] We're ready for this issue to be reported to the central authority (i.e., make this public now)
- [ ] (when applicable) The GitHub Security Advisory is ready to be published now

Note that for those projects that host their repositories on GitHub, the use of GitHub Security Advisories is recommended but is not required.

**This section will be completed by the EMO**.

**CVE:** {cve}

- [ ] All required information is provided
- [ ] CVE Assigned
- [ ] Pushed to Mitre
- [ ] Accepted by Mitre

<!-- Quick actions will configure the state of the issue. Leave these. -->
/confidential
/label ~"CVE Assignment"
/assign @wbeaton
